#ifndef _I2C_STATE_MACHINE_
#define _I2C_STATE_MACHINE_

#define num_i2c_dev 23

typedef void(*tx_interrupt_callback)(void);
typedef void(*rx_interrupt_callback)(void);
typedef void(*buffer_full_interrupt_callback)(void);
typedef void(*buffer_overflow_interrupt_callback)(void);

typedef enum
{
	good	=0,
	no_good,
	pretty_fucked,
	pity_fucked,

	invalid,
	trturn_type_status_num=	invalid
	
}return_type_status;

typedef enum
{
	absent		=0,
	present,
	
	invalid,
	device_presence_num

}device_presence_e;	/*presence of device*/

typedef enum
{
	buffer_empty	=0,
	buffer_full,
	buffer_overflow,
	buffalo_fucked,

	invalid,
	buffer_state_num=invalid

}buffer_state_e;

typedef enum
{
	not_initialized		=0,
	hardware_initialized,
	hardware_initialized,
	software_configured,
	software_initialized,

	state_machine_initialized	=	software_initialized,
	invalid_State,
	buffer_state_num

}state_sm_e;

typedef struct
{
	inactive	=0,
	active,
	active_being_served

	invalid,
	interrupt_state_num=	invalid
	
}interrupt_state_e;	/*interrupt states*/

typedef struct
{
	interrupt_state_e state;
	union
	{
		tx_interrupt_callback 		tx_ISR;
		rx_interrupt_callback 		rx_ISR;
		buffer_full_interrupt_callback 		buffer_full_ISR;
		buffer_overflow_callback 	buffer_overflow_ISR;
	}interrupt_callback;

}i2c_interrupt_s;

typedef struct
{
	boolean state;	/*1 mean good 0 mean bad*/
	boolean working_mode;	/*1 mean master or 0 mean slave*/
	buffer_state_e i2c_tx_buffer_state;
	buffer_state_e i2c_rx_buffer_state;
	i2c_interrupt_s i2c_interrupt[i2c_interrupt_type_num];

}i2c_state_per_device_s;

typedef struct
{
	state_sm_e state;
	device_presence_e device_presence[num_i2c_dev];

}i2c_sm_s;

return_type_status allocate_memory_for_i2c_sm(void);

return_type_status set_state_machine_to_default(void);
