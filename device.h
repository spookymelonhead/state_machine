#ifndef _DEVICE_H_
#define _DEVICE_H_

typedef void(*device_soft_init_routines_t)(void);
typedef void(*device_hard_init_routines_t)(void);

typedef void(*device_soft_deinit_routines_t)(void);
typedef void(*device_hard_deinit_routines_t)(void);

typedef void(*device_init_t)(void);
typedef void(*device_deinit_t)(void);

typedef enum
{
	spi		=0,
	timer,
	i2c,

	invalid_device,
	device_num=		invalid_device
	
}device_e;

typedef enum
{
	uninitialized		=0,
	memory_allocated,
	software_initialized,
	hardware_iniialized,

	invalid,
	device_state_num=		invalid	

}device_sm_state_e;

typedef struct
{
	device_hard_init_routines_t device_soft_init;
	device_soft_init_routines_t	device_hard_init;

}device_init_routines_s;

typedef struct
{
	device_hard_deinit_routines_t device_soft_deinit;
	device_soft_deinit_routines_t device_hard_deinit;

}device_deinit_routines_s;

typdef struct
{
	void *device;
	device_init_routines_s device_init_routines;
	device_deinit_routines_s device_init_routine;
	
}device_s;


typedef struct
{
	device_init_t device_init;
	device_deinit_t device_deinit;
	
}device_sm_init_routines_s;

typedef struct
{
	device_sm_state_e state;
	device_sm_init_routines_s init_routines;
	device_s device[device_num];
	
	
}device_sm_s;

extern device_sm_s device_sm;